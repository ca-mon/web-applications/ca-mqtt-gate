package fr.camon.caserver.entities;

import inet.ipaddr.mac.MACAddress;
import lombok.*;

import java.net.InetAddress;
import java.time.Instant;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Device {

    String name;
    String firmwareVersion;
    MACAddress macAddress;
    InetAddress inetAddress;
    Instant lastSeen;

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Device)) {
            return false;
        }
        Device d = (Device) obj;
        return d.getName().equals(name)
                && d.getFirmwareVersion().equals(firmwareVersion)
                && d.getInetAddress().equals(inetAddress)
                && d.getMacAddress().equals(macAddress);
    }
}
