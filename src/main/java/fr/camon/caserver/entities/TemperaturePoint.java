package fr.camon.caserver.entities;

import lombok.*;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Measurement(name = "temperature")
public class TemperaturePoint {
    @Column(name = "time")
    private Instant time;

    @Column(name = "device")
    private String mac;

    @Column(name = "temperature")
    private double value;
}
