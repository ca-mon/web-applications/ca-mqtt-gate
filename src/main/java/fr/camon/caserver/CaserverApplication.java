package fr.camon.caserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CaserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(CaserverApplication.class, args);
    }
}
