package fr.camon.caserver.repositories;

import fr.camon.caserver.config.influx.InfluxDbProperties;
import lombok.extern.log4j.Log4j2;
import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;
import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Log4j2
@EnableConfigurationProperties(InfluxDbProperties.class)
public class InfluxDBRepository {

    private final InfluxDB influxDB;

    @Autowired
    public InfluxDBRepository(InfluxDbProperties properties, InfluxDB influxDB) {
        this.influxDB = influxDB;

        log.warn(properties.getDatabase());
        log.warn(properties.getRetention_policy());
        influxDB.setDatabase(properties.getDatabase());

        influxDB.enableBatch(BatchOptions.DEFAULTS.exceptionHandler(
                (failedPoints, throwable) -> { /* custom error handling here */ })
        );

        influxDB.setRetentionPolicy(properties.getRetention_policy());
    }

    public void write(List<Point> points) {
        for (Point p : points) influxDB.write(p);
    }

    public void write(Point point) {
        influxDB.write(point);
    }

    public QueryResult query(Query query) {
        return influxDB.query(query);
    }

    public Pong ping() {
        return influxDB.ping();
    }
}
