package fr.camon.caserver.config.influx;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class RandomDataConfiguration {
    @Bean
    Random random() {
        return new Random();
    }
}
