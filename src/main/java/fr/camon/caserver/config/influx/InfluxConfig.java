package fr.camon.caserver.config.influx;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@Log4j2
@EnableConfigurationProperties(InfluxDbProperties.class)
public class InfluxConfig {
    @Bean
    InfluxDbProperties properties(InfluxDbProperties properties) {
        return properties;
    }
}
