package fr.camon.caserver.config.influx;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.influx")
public class InfluxDbProperties {

    /**
     * URL of the InfluxDB instance to which to connect.
     */
    private String url;

    /**
     * Login user.
     */
    private String user;

    /**
     * Login password.
     */
    private String password;

    /**
     * Login database.
     */
    private String database;


    /**
     * Login retention_policy.
     */
    private String retention_policy;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return this.database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getRetention_policy() {
        return retention_policy;
    }

    public void setRetention_policy(String retention_policy) {
        this.retention_policy = retention_policy;
    }

}
