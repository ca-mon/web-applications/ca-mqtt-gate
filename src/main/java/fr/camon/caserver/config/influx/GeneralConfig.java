package fr.camon.caserver.config.influx;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GeneralConfig {

    @Bean
    Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create();
    }
}
