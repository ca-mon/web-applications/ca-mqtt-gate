package fr.camon.caserver.service.sensor;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class SensorService {

    private final HumiditySensorService humiditySensorService;
    private final TemperatureSensorService temperatureSensorService;

    @Autowired
    public SensorService(HumiditySensorService humiditySensorService, TemperatureSensorService temperatureSensorService) {
        this.humiditySensorService = humiditySensorService;
        this.temperatureSensorService = temperatureSensorService;
    }

    public void handleSensorValues(String topic, String message) {
        if (topic.contains("humidity")) {
            humiditySensorService.sendInflux(topic, message);
        } else if (topic.contains("temperature")) {
            temperatureSensorService.sendInflux(topic, message);
        } else {
            log.error("Unhandled sensor topic : " + topic);
        }
    }
}
