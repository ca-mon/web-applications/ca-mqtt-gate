package fr.camon.caserver.service.sensor;

import fr.camon.caserver.service.influx.InfluxDBService;
import org.influxdb.dto.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

import static fr.camon.caserver.service.CaserverService.getMacFromMessage;

@Service
public class TemperatureSensorService implements CASensor {

    private final InfluxDBService influxDBService;

    @Autowired
    public TemperatureSensorService(InfluxDBService influxDBService) {
        this.influxDBService = influxDBService;
    }

    public void sendInflux(String topic, String message) {
        String influxTemperatureField = "temperature";
        Point p = Point.measurement(this.parseMeasurement(topic))
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .tag("device", getMacFromMessage(message))
                .addField(influxTemperatureField, Long.parseLong(this.parseValue(message)))
                .build();
        influxDBService.savePoint(p);
    }

    public String parseMeasurement(String topic) {
        if (topic.equals("home/sensor/temperature")) {
            return "temperature";
        }
        throw new RuntimeException("Topic unrecognized to send temperature -> topic is : " + topic);
    }
}
