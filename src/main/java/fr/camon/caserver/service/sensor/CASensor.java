package fr.camon.caserver.service.sensor;

public interface CASensor {
    void sendInflux(String topic, String message);

    String parseMeasurement(String topic);

    default String parseValue(String msg) {
        String[] split = msg.split("@");
        if (split.length == 2) {
            return split[1];
        }
        throw new RuntimeException("Message is not of the valid form.");
    }
}
