package fr.camon.caserver.service.influx;

import fr.camon.caserver.entities.TemperaturePoint;
import fr.camon.caserver.repositories.InfluxDBRepository;
import lombok.extern.log4j.Log4j2;
import org.influxdb.dto.Point;
import org.influxdb.dto.Pong;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Log4j2
@Service
public class InfluxDBService {

    private final InfluxDBRepository influxDBRepository;

    @Autowired
    public InfluxDBService(InfluxDBRepository influxDBRepository) {
        this.influxDBRepository = influxDBRepository;
        log.info("InfluxDB Version " + influxDBRepository.ping().getVersion());
    }

    public boolean influxHealth() {
        Pong ping = influxDBRepository.ping();
        log.info("InfluxDB Server : " + ping.getVersion());
        if (ping.isGood()) {
            log.info("Server Response time : " + ping.getResponseTime() + "ms");
        } else {
            log.error("Unable to contact the server");
        }
        return ping.isGood();
    }

    @Async
    public void savePoint(Point point) {
        influxDBRepository.write(point);
    }

    @Async
    public CompletableFuture<List<TemperaturePoint>> getTemperaturePointValues() throws Exception {
        InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
        Query query = new Query("SELECT value from dev_mqtt.autogen.temperature");


        long startTime = System.currentTimeMillis();
        QueryResult allGaussValues = influxDBRepository.query(query);
        long endTime = System.currentTimeMillis();
        long timeElapsed = endTime - startTime;

        List<TemperaturePoint> pointDtoGauss = resultMapper
                .toPOJO(allGaussValues, TemperaturePoint.class);

        log.info(pointDtoGauss.size() + " points have been retrieved / Execution time in milliseconds: " + timeElapsed);
        return CompletableFuture.completedFuture(pointDtoGauss);
    }
}
