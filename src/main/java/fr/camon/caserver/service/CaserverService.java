package fr.camon.caserver.service;

import fr.camon.caserver.service.device.DeviceService;
import fr.camon.caserver.service.mqtt.MessagingService;
import fr.camon.caserver.service.sensor.SensorService;
import inet.ipaddr.AddressStringException;
import inet.ipaddr.MACAddressString;
import lombok.extern.log4j.Log4j2;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CaserverService implements CommandLineRunner {

    private final MessagingService messagingService;
    private final DeviceService deviceService;
    private final SensorService sensorService;

    @Autowired
    public CaserverService(MessagingService messagingService, DeviceService deviceService, SensorService sensorService) {
        this.messagingService = messagingService;
        this.deviceService = deviceService;
        this.sensorService = sensorService;
    }

    public static String getMacFromMessage(String message) {
        String[] split = message.split("@");
        if (split.length > 0) {
            return new MACAddressString(split[0].toUpperCase()).toString().toUpperCase();
        }
        throw new RuntimeException("Unable to parse mac address from message : " + message);
    }

    @Scheduled(fixedDelay = 5000)
    public void scheduleFixedDelayTask() {
        deviceService.getDeviceList().forEach(device -> {
            try {
                deviceService.askDeviceInfos(device.getMacAddress().toString());
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
    }

    private void handleMqttMessagesCaServer(String topic, String message) throws MqttException {
        if (isAuthorized(message)) {
            sensorService.handleSensorValues(topic, message);
        } else deviceService.askDeviceInfos(getMacFromMessage(message));
    }

    private void handleRegisterDevice(String topic, String message) throws AddressStringException, UnknownHostException {
        deviceService.registerDevice(topic, message);
    }

    private void handleUnRegisteredMqttMessages(String topic, String message) {
        List<String> collect = messagingService.getTopic_subscribed().stream().filter(tpc -> {
            if (tpc.equals("#")) return false;
            String[] split = tpc.split("#");
            return tpc.equals(topic) || (tpc.contains("#") && topic.contains(split[0]));
        }).collect(Collectors.toList());
        if (collect.size() == 0) log.debug("Received unhandled Topic : " + topic + " -> Message is : " + message);
    }

    @Override
    public void run(String... args) throws Exception {
        messagingService.subscribe("home/sensor/#", this::handleMqttMessagesCaServer, 0);
        messagingService.subscribe("ESP/ClientID", this::handleRegisterDevice, 2);
        messagingService.subscribe("#", this::handleUnRegisteredMqttMessages, 0);
    }

    private boolean isAuthorized(String message) {
        boolean b = deviceService.getDeviceList()
                .stream()
                .filter(device -> device.getMacAddress().toString().toUpperCase().equals(getMacFromMessage(message)))
                .count() == 1;
        if (b) {
            log.debug("message : " + message + " has been authorized");
        } else log.warn("message : " + message + " is unauthorized");
        return b;
    }
}