package fr.camon.caserver.service.device;

import fr.camon.caserver.entities.Device;
import fr.camon.caserver.service.mqtt.MessagingService;
import inet.ipaddr.AddressStringException;
import inet.ipaddr.MACAddressString;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class DeviceService {

    private final MessagingService messagingService;
    @Getter
    private List<Device> deviceList;

    @Autowired
    public DeviceService(MessagingService messagingService) {
        this.messagingService = messagingService;
        this.deviceList = new ArrayList<>();
    }

    public void register(Device device) {
        Optional<Device> first = deviceList.stream().filter(d -> d.equals(device)).findFirst();
        if (first.isEmpty()) {
            deviceList.add(device);
            return;
        }
        deviceList = deviceList.stream()
                .map(d -> d.getMacAddress().equals(device.getMacAddress()) ? device : d
                ).collect(Collectors.toList());
    }

    public void registerDevice(String topic, String msg) throws UnknownHostException, AddressStringException {
        String[] split = msg.split("@");
        if (split.length != 4) return;
        Device build = Device.builder()
                .name(split[2])
                .firmwareVersion(split[3])
                .macAddress(new MACAddressString(split[0]).toAddress())
                .inetAddress(InetAddress.getByName(split[1]))
                .lastSeen(Instant.now())
                .build();
        this.register(build);
    }

    public void askDeviceInfos(String macAddr) throws MqttException {
        messagingService.publish("ESP/status/ping", macAddr.toUpperCase(), 2, false);
    }
}
