package fr.camon.caserver.service.mqtt;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class MessagingService {

    private final IMqttClient mqttClient;

    @Getter
    private final List<String> topic_subscribed;

    @Autowired
    public MessagingService(IMqttClient mqttClient) {
        this.mqttClient = mqttClient;
        this.topic_subscribed = new ArrayList<>();
    }

    public void publish(final String topic, final String payload, int qos, boolean retained)
            throws MqttException {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setPayload(payload.getBytes());
        mqttMessage.setQos(qos);
        mqttMessage.setRetained(retained);
        //   mqttClient.publish(topic, mqttMessage);
        mqttClient.publish(topic, payload.getBytes(), qos, retained);
    }

    public void subscribe(final String topic) throws MqttException {
        if (!topic_subscribed.contains(topic)) this.topic_subscribed.add(topic);
        mqttClient.subscribeWithResponse(topic, (tpic, msg) -> {
            String message = new String(msg.getPayload());
            log.info(tpic + " -> " + message);
            // handleMqttMessagesCaServer(tpic, message);
        });
    }

    public void subscribe(final String topic, MqttMethod mqttMethod) throws MqttException {
        if (!topic_subscribed.contains(topic)) this.topic_subscribed.add(topic);
        mqttClient.subscribeWithResponse(topic, (tpic, msg) -> {
            String message = new String(msg.getPayload());
            mqttMethod.operate(tpic, message);
        });
    }

    public void subscribe(final String topic, MqttMethod mqttMethod, int qos) throws MqttException {
        if (!topic_subscribed.contains(topic)) this.topic_subscribed.add(topic);
        mqttClient.subscribeWithResponse(topic, qos, (tpic, msg) -> {
            String message = new String(msg.getPayload());
            mqttMethod.operate(tpic, message);
        });
    }

}
